package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

public class EmployeeOps {

	public Employee addEmployee(Employee emp) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
				
		String insertQry = "insert into employee " + 
		"(empName, salary, gender, emailId, password) values " + 
		"(?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			
			int result = pst.executeUpdate();
			
			if (result > 0) {
				
				pst.close();
				con.close();
				
				con = DbConnection.getConnection();
				pst = con.prepareStatement("Select * from employee where emailId = ?");
				pst.setString(1, emp.getEmailId());
				ResultSet rs = pst.executeQuery();
				
				if (rs.next()) {
					Employee employee = new Employee();
					employee.setEmpId(rs.getInt(1));
					employee.setEmpName(rs.getString(2));
					employee.setSalary(rs.getDouble(3));
					employee.setGender(rs.getString(4));
					employee.setEmailId(rs.getString(5));
					employee.setPassword(rs.getString(6));
					return employee;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return null;
	}

}