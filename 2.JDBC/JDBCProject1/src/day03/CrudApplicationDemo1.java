package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

class EmployeeOperations {
	
	public String addEmployee(String empName, double salary, String gender, String emailId, String password) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			return "Unable to Establish the Connection";
		}
			
		String insertQry = "insert into employee " + 
		"(empName, salary, gender, emailId, password) values " + 
		"(?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, empName);
			pst.setDouble(2, salary);
			pst.setString(3, gender);
			pst.setString(4, emailId);
			pst.setString(5, password);
			
			int result = pst.executeUpdate();
			
			if (result > 0)
				return "Record Inserted into the Table..";
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
		
		return "Failed to Insert the Record!!!";
	}

	public String updateEmployee(int empId, double salary) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		

		if (con == null) {
			return "Unable to Establish the Connection";
		}
		
		String updateQry = "update employee set salary = ? where empId = ?";
		
		try {
			pst = con.prepareStatement(updateQry);
			pst.setDouble(1, salary);
			pst.setInt(2, empId);			
			int result = pst.executeUpdate();
			
			if (result > 0)
				return "Record Updated Successfully";
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return "Failed to Update the Record";
	}

	public String deleteEmployee(int empId) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		

		if (con == null) {
			return "Unable to Establish the Connection";
		}
		
		String deleteQry = "delete from employee where empId = ?";
		
		try {
			pst = con.prepareStatement(deleteQry);
			pst.setInt(1, empId);			
			int result = pst.executeUpdate();
			
			if (result > 0)
				return "Record Deleted Succeefully";
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return "Failed to Delete the Record";
	}

	public void showAllEmployees() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		try {
			pst = con.prepareStatement("Select * from employee");
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {
				System.out.print(rs.getInt(1) + " ");
				System.out.print(rs.getString(2) + " ");
				System.out.print(rs.getDouble(3) + " ");
				System.out.print(rs.getString(4) + " ");
				System.out.print(rs.getString(5) + " ");
				System.out.println();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void showEmpById(int empId) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		String selectQry = "Select * from employee where empId = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, empId);
			ResultSet rs = pst.executeQuery();
			
			if (rs.next()) {
				System.out.println("EmpId   : " + rs.getInt(1));
				System.out.println("EmpName : " + rs.getString(2));
				System.out.println("Salary  : " + rs.getDouble("salary"));
				System.out.println("Gender  : " + rs.getString(4));
				System.out.println("Email-Id: " + rs.getString(5));
				System.out.println();
			} else {
				System.out.println("Employee Record Not Found!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void showEmpByName(String empName) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		try {
			pst = con.prepareStatement("Select * from employee where empName = ?");
			pst.setString(1, empName);
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {
				System.out.print(rs.getInt(1) + " ");
				System.out.print(rs.getString(2) + " ");
				System.out.print(rs.getDouble(3) + " ");
				System.out.print(rs.getString(4) + " ");
				System.out.print(rs.getString(5) + " ");
				System.out.println();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String empLogin(String emailId, String password) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			return "Unable to Establish the Connection";
		}
				
		String selectQry = "Select * from employee where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			ResultSet rs = pst.executeQuery();
			
			if (rs.next()) {
				return "Login Success";
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return "Failed to Login!!!";
	}

	public void showEmpByEmailId(String emailId) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		try {
			pst = con.prepareStatement("Select * from employee where emailId = ?");
			pst.setString(1, emailId);
			ResultSet rs = pst.executeQuery();
			
			if (rs.next()) {
				System.out.println("EmpId   : " + rs.getInt(1));
				System.out.println("EmpName : " + rs.getString(2));
				System.out.println("Salary  : " + rs.getDouble(3));
				System.out.println("Gender  : " + rs.getString(4));
				System.out.println("Email-Id: " + rs.getString(5));
				System.out.println();
			} else {
				System.out.println("Employee Not Found!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}


public class CrudApplicationDemo1 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		EmployeeOperations empOps = new EmployeeOperations();
		
		while (true) {
			System.out.println("Select Your Choice");
			System.out.println("------------------");
			System.out.println("1. Add Employee");
			System.out.println("2. Update Employee");
			System.out.println("3. Delete Employee");
			System.out.println("4. Show All Employees");
			System.out.println("5. Show Employee By Id");
			System.out.println("6. Show Employee By Name");
			System.out.println("7. Show Employee By Email-Id");
			System.out.println("8. Employee Login");
			System.out.println("9. Exit \n");
			
			System.out.print("Enter Your Choice: ");
			int choice = scan.nextInt();
			System.out.println();
			
			switch(choice) {
			
			case 1:
				System.out.println("Enter EmpName, Salary," + 
						" Gender, EmailId and Password ");
				
				System.out.println(empOps.addEmployee(scan.next(), 
						scan.nextDouble(), scan.next(), 
						scan.next(), scan.next()));
				System.out.println();
			break;
			
			case 2:
				System.out.print("Enter EmpId : ");
				int empId = scan.nextInt();
				System.out.print("Enter Salary: ");
				double salary = scan.nextDouble();
				System.out.println();
				System.out.println(empOps.updateEmployee(empId, salary));
			break;
			
			case 3:
				System.out.print("Enter EmpId : ");
				empId = scan.nextInt();
				System.out.println();
				System.out.println(empOps.deleteEmployee(empId));
			break;
				
			case 4:empOps.showAllEmployees();
			break;
				
			case 5:
				System.out.print("Enter EmpId : ");
				empId = scan.nextInt();
				System.out.println();
				empOps.showEmpById(empId);
			break;
				
			case 6:
				System.out.print("Enter EmpName : ");
				String empName = scan.next();
				System.out.println();
				empOps.showEmpByName(empName);
			break;
			
			case 7:
				System.out.print("Enter Email-Id : ");
				String emailId = scan.next();
				System.out.println();
				empOps.showEmpByEmailId(emailId);
			break;
				
			case 8:
				System.out.print("Enter EmailId : ");
				emailId = scan.next();
				System.out.print("Enter Password: ");
				String password = scan.next();
				System.out.println();
				System.out.println(empOps.empLogin(emailId, password));
			break;
				
			case 9: System.out.println("Application Terminated...");
					//System.exit(0);
					return;
			
			default:System.out.println("Invalid Choice!!!");
			break;
			}
			
			System.out.println();
		}
		

	}

}
