package day03;

import java.util.Scanner;

public class Assignment3 {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		EmployeeOps empOps = new EmployeeOps();
		
		while (true) {
			System.out.println("Select Your Choice");
			System.out.println("------------------");
			System.out.println("1. Add Employee");
			System.out.println("2. Update Employee");
			System.out.println("3. Delete Employee");
			System.out.println("4. Show All Employees");
			System.out.println("5. Show Employee By Id");
			System.out.println("6. Show Employee By Name");
			System.out.println("7. Show Employee By Email-Id");
			System.out.println("8. Employee Login");
			System.out.println("9. Exit \n");
			
			System.out.print("Enter Your Choice: ");
			int choice = scan.nextInt();
			System.out.println();
			
			switch(choice) {
			
			case 1:
				System.out.println("Enter EmpName, Salary," + 
						" Gender, EmailId and Password ");
				
				Employee emp = new Employee(scan.next(), scan.nextDouble(), 
						scan.next(), scan.next(), scan.next());
				
				emp = empOps.addEmployee(emp);
				
				if (emp != null) {
					System.out.println("Record Inserted Successfully with Id: " + emp.getEmpId());
					System.out.println(emp);
				} else {
					System.out.println("Failed to Insert the Employee Record");
				}
				
			break;
			
			case 2:
				
			break;
			
			case 3:
				
			break;
				
			case 4:
			break;
				
			case 5:
				
			break;
				
			case 6:
				
			break;
			
			case 7:
				
			break;
				
			case 8:
				
			break;
				
			case 9: System.out.println("Application Terminated...");
					//System.exit(0);
					return;
			
			default:System.out.println("Invalid Choice!!!");
			break;
			}
			
			System.out.println();
		}
	}
}