package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class SelectDemo2 {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter EmpId: ");
		int empId = scan.nextInt();
		
		String selectQry = "select * from employee where empId = ?" ;
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, empId);
			ResultSet rs = pst.executeQuery();  
			if (rs.next()) {
				System.out.println("EmpId   : " + rs.getInt(1));
				System.out.println("EmpName : " + rs.getString(2));
				System.out.println("Salary  : " + rs.getDouble("salary"));
				System.out.println("Gender  : " + rs.getString(4));
				System.out.println("Email-Id: " + rs.getString(5));
				System.out.println();
			} else {
				System.out.println("Employee Record Not Found!");
			} 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}
