package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertDemo1 {
	public static void main(String[] args) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		if (con == null) {
			System.out.println("Unable to Establish the Connection");
			return;
		}
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter EmpName, Salary, Gender, EmailId and Password ");
		String empName = scan.next();
		double salary = scan.nextDouble();
		String gender = scan.next();
		String emailId = scan.next();
		String password = scan.next();
		System.out.println();
		
		String insertQry = "insert into employee " + 
		"(empName, salary, gender, emailId, password) values " + 
		"(?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, empName);
			pst.setDouble(2, salary);
			pst.setString(3, gender);
			pst.setString(4, emailId);
			pst.setString(5, password);
			
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Inserted into the Table");
			} else {
				System.out.println("Failed to Insert the Record");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}
